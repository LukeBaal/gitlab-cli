const axios = require('axios');
const keys = require('./config/keys');

const newProject = options => {
  axios
    .post('https://gitlab.com/api/v4/projects', options, {
      headers: {
        'Private-Token': keys.PERSONAL_ACCESS_TOKEN
      }
    })
    .then(res => {
      addReadme(res.data.id, options.name, options.description);
      console.info(`Project created at ${res.data.web_url}`);
    })
    .catch(err => console.log(err));
};

const addReadme = (id, name, description) => {
  const content = `# ${name[0].toUpperCase()}${name.slice(1)}\n${description}`;
  axios
    .post(
      `https://gitlab.com/api/v4/projects/${id}/repository/files/README.md`,
      {
        branch: 'master',
        commit_message: 'first commit',
        content
      },
      {
        headers: {
          'Private-Token': keys.PERSONAL_ACCESS_TOKEN
        }
      }
    )
    .then(res => console.info('README added'))
    .catch(err => console.log(err));
};

const listProjects = () => {
  axios
    .get(`https://gitlab.com/api/v4/users/${keys.USERNAME}/projects`, {
      headers: {
        'Private-Token': keys.PERSONAL_ACCESS_TOKEN
      }
    })
    .then(res => {
      res.data.forEach(project => {
        const { name, description, web_url, last_activity_at } = project;
        console.log({ name, description, web_url, last_activity_at });
      });
    });
};

module.exports = {
  newProject,
  listProjects
};
