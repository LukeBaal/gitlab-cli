#!/usr/bin/env node
const program = require('commander');
const { prompt } = require('inquirer');
const { newProject, listProjects } = require('./index');

const questions = [
  {
    type: 'input',
    name: 'name',
    messsage: 'Enter the name for the project:'
  },
  {
    type: 'list',
    name: 'visibility',
    message: 'Visibility:',
    choices: ['private', 'internal', 'public']
  },
  {
    type: 'input',
    name: 'description',
    message: 'Description'
  }
];

// {
//   type: 'confirm',
//   name: 'readme',
//   message: 'Add a README?'
// }

program.version('1.0.0').option('Gitlab CLI');

// New project command
program
  .command('new')
  .alias('n')
  .description(
    'Create a new project on gitlab and adds a README with the given project name and description'
  )
  .action(() => {
    prompt(questions).then(answers => {
      if (answers.name.length > 0) {
        newProject(answers);
      }
    });
  });

program
  .command('list')
  .alias('ls')
  .description('List all projects')
  .action(() => {
    listProjects();
  });

program.parse(process.argv);
