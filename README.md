# Gitlab-cli
CLI tool to create a new project on gitlab w/ a readme from the command line

## How To use
Add the following to the file config/keys.js

```javascript
module.exports = {
    PERSONAL_ACCESS_TOKEN: 'my_token'
}
```

replacing 'my_token' with your personal access token to your gitlab account, which can be generated at: account settings > access tokens